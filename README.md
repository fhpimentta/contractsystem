<h1 align="center">
	<img alt="ContratoSystem" src="/frontend/src/assets/logo.png" width="200px" />
</h1>

<h3 align="center">
  Contrato System
</h3>



## 📥 Instalação e execução

1. Faça um clone desse repositório.
2. Instalar <a href='https://nodejs.org/en/'>NodeJs</a>
2. Instalar <a href='https://yarnpkg.com/getting-started/install'>Yarn</a> ( é possivel rodar o projeto apenas com o npm)
3. Instalar <a href='https://www.notion.so/Instalando-Docker-6290d9994b0b4555a153576a1d97bee2'>DOCKER</a>


### Banco de Dados
1. A partir da raiz do terminal executar 
  `docker run --name contrato-postgres -e POSTGRES_PASSWORD=contratosystem -p 5432:5432 -d postgres`

### Backend

1. A partir da raiz do projeto, entre na pasta do backend rodando `cd backend`;
2. Rode `yarn install` para instalar as dependências;
5. Rode `yarn typeorm migration:run` para executar as migrations;
6. Rode `yarn dev:server` para iniciar o servidor.

### Frontend Web

_ps: Antes de executar, lembre-se de iniciar o backend deste projeto_

1. A partir da raiz do projeto, entre na pasta do frontend web rodando `cd frontend`;
2. Rode `yarn install` para instalar as dependências;
3. Rode `yarn start` para iniciar o client.



Feito com ♥ by [FernandoPimenta](https://www.linkedin.com/in/fepimenta/)
