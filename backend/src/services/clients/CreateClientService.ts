import { getRepository } from 'typeorm';
import Clients from '../../models/Clients';
import AppError from '../../erros/AppError';

interface Request {
  name: string;
  surname: string;
  cpf: string;
  phone: string;
  email: string;
}

class CreateClientsService {
  public async execute({
    name,
    surname,
    cpf,
    phone,
    email,
  }: Request): Promise<Clients> {
    const clientsRepository = getRepository(Clients);

    const checkUserExists = await clientsRepository.findOne({
      where: { cpf },
    });

    if (checkUserExists) {
      throw new AppError('Cpf address already used.');
    }

    const clients = clientsRepository.create({
      name,
      surname,
      cpf,
      phone,
      email,
    });
    
    await clientsRepository.save(clients);

    return clients;
  }
}

export default CreateClientsService;
