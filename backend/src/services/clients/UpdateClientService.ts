import { getRepository } from 'typeorm';
import Clients from '../../models/Clients';
import AppError from '../../erros/AppError';

interface Request {
  id: string;
  name: string;
  surname: string;
  cpf: string;
  phone: string;
  email: string;
}

class UpdateClientsService {
  public async execute({
    id,
    name,
    surname,
    cpf,
    phone,
    email,
  }: Request): Promise<Clients> {
    const clientsRepository = getRepository(Clients);

    const client = await clientsRepository.findOne({
      where: { id },
    });
    
    if (!client) {
      throw new AppError('Cliente não existe!');
    }

    client.name = name;
    client.surname = surname;
    client.email = email;
    client.cpf = cpf;
    client.phone = phone;

    await clientsRepository.update(id, {
      name,
      surname,
      cpf,
      email,
      phone,
    });

    return client;
  }
}

export default UpdateClientsService;
