/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import { getRepository } from 'typeorm';
import Clients from '../../models/Clients';
import Contracts from '../../models/Contracts';
import ClientContracts from '../../models/ClientContracts';

class GetClientContractsService {
  public async execute(_id: string): Promise<Contracts[]> {
    const contractsRepository = getRepository(Contracts);
    const clientsContractsRepository = getRepository(ClientContracts);
   
    const clientContracts = await clientsContractsRepository.find({
      where: { client_id: _id },
    });

    const list: Contracts[] = [];

    for (const c of clientContracts) {
      const contract = await contractsRepository.findOne({
        where: { id: c.contract_id },
      });
      console.log(c);
      if (contract !== undefined) {
        list.push(contract);
      }
    }

    return list;
  }
}

export default GetClientContractsService;
