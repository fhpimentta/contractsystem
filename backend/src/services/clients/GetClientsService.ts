import { getRepository } from 'typeorm';
import Clients from '../../models/Clients';

class GetClientsService {
  public async execute(_id: string): Promise<Clients[]> {
    const clientsRepository = getRepository(Clients);

    const clients =
     ( _id) !== ''
        ? [await clientsRepository.findOne({ where: { id: _id } })]
        : await clientsRepository.find();

    return clients;
  }
}

export default GetClientsService;
