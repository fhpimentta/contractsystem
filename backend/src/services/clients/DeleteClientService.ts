import { getRepository } from 'typeorm';
import Clients from '../../models/Clients';
import AppError from '../../erros/AppError';

interface Request {
  id: string;
}

class DeleteClientsService {
  public async execute({ id }: Request): Promise<Clients> {
    const clientsRepository = getRepository(Clients);

    const client = await clientsRepository.findOne({
      where: { id },
    });

    if (!client) {
      throw new AppError('Cliente não existe!');
    }

    await clientsRepository.delete(id);

    return true;
  }
}

export default DeleteClientsService;
