import { getRepository } from 'typeorm';
import AppError from '../../erros/AppError';
import Contracts from '../../models/Contracts';

interface Request {
  id: string;
}

class DeleteContractService {
  public async execute({ id }: Request): Promise<Contracts> {
    const contractRepository = getRepository(Contracts);

    const client = await contractRepository.findOne({
      where: { id },
    });

    if (!client) {
      throw new AppError('Contrato não existe!');
    }

    await contractRepository.delete(id);

    return true;
  }
}

export default DeleteContractService;
