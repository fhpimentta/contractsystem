/* eslint-disable camelcase */
import { getRepository } from 'typeorm';
import path from 'path';
import fs from 'fs';
import uploadConfig from '../../config/upload';
import AppError from '../../erros/AppError';
import Contracts from '../../models/Contracts';

interface Request {
  contract_id: string;
  filename: string;
}

class UpdateContractFileService {
  public async execute({ contract_id, filename }: Request): Promise<Contracts> {
    const contractsRepository = getRepository(Contracts);
   
    const contract = await contractsRepository.findOne(contract_id);

    if (!contract) {
      throw new AppError('Contrato Bloqueado para esse tipo de usuario!', 401);
    }

    if (contract.file) {
      // deletar file anterior

      const contractFilePath = path.join(uploadConfig.directory, contract.file);
      const contractFileExists = await fs.promises.stat(contractFilePath);

      if (contractFileExists) {
        await fs.promises.unlink(contractFilePath);
      }
    }
    contract.file = filename;
    await contractsRepository.save(contract);
    return contract;
  }
}

export default UpdateContractFileService;
