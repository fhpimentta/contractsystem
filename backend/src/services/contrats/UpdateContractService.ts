import { getRepository } from 'typeorm';
import { startOfHour } from 'date-fns';
import AppError from '../../erros/AppError';
import Contracts from '../../models/Contracts';
import ClientContracts from '../../models/ClientContracts';

interface Request {
  id: string;
  title: string;
  initial_date: Date;
  final_date: Date;
  clients: string[];
}

class UpdateContractService {
  public async execute({
    id,
    title,
    initial_date,
    final_date,
    clients,
  }: Request): Promise<Contracts> {
    const clientsRepository = getRepository(Contracts);
    const contract = await clientsRepository.findOne({
      where: { id },
    });

    if (!contract) {
      throw new AppError('Contrato não existe!');
    }
    const i = startOfHour(initial_date);
    const f = startOfHour(final_date);

    contract.title = title;
    contract.initial_date = i;
    contract.final_date = f;

    await clientsRepository.update(id, {
      title,
      initial_date: i,
      final_date: f,
    });

    // update client and contracts relations
    const clientContractRepository = getRepository(ClientContracts);
    const relations = await clientContractRepository.find({
      where: { contract_id: contract.id },
    });
console.log(clients);
    clients.forEach(async (client) => {
      const clientHasContract = relations.find((r) => {
        return client === r.client_id;
      });

      if (clientHasContract === undefined) {
        const c = clientContractRepository.create({
          client_id: client,
          contract_id: contract.id,
        });

        await clientContractRepository.save(c);
      }
    });

    relations.forEach(async (client) => {
      const clientHasContract = clients.find((r) => {
        return client.client_id === r;
      });

      if (clientHasContract === undefined) {
        await clientContractRepository.delete(client.id);
      }
    });

    return contract;
  }
}

export default UpdateContractService;
