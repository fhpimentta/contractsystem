import { getRepository } from 'typeorm';
import { startOfHour } from 'date-fns';
import Contracts from '../../models/Contracts';
import ClientContracts from '../../models/ClientContracts';

interface Request {
  title: string;
  initial_date: Date;
  final_date: Date;
  clients: string[];
}

class CreateContractsService {
  public async execute({
    title,
    initial_date,
    final_date,
    clients,
  }: Request): Promise<Contracts> {
    const contractRepository = getRepository(Contracts);
    const clientContractRepository = getRepository(ClientContracts);

    const i = startOfHour(initial_date);
    const f = startOfHour(final_date);

    const contract = contractRepository.create({
      title,
      initial_date: i,
      final_date: f,
      file: '',
    });

    await contractRepository.save(contract);

    clients.forEach(async (client) => {
      const c = clientContractRepository.create({
        client_id: client,
        contract_id: contract.id,
      });

      await clientContractRepository.save(c);
    });

    return contract;
  }
}

export default CreateContractsService;
