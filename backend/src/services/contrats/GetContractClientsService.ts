/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import { getRepository } from 'typeorm';
import Clients from '../../models/Clients';
import ClientContracts from '../../models/ClientContracts';

class GetContractsClientsService {
  public async execute(_id: string): Promise<Clients[]> {
    const contractRepository = getRepository(ClientContracts);
    const clientRepository = getRepository(Clients);

    const contracts = await contractRepository.find({
      where: { contract_id: _id },
    });
    const list: Clients[] = [];

    for (const c of contracts) {
      const client = await clientRepository.findOne({
        where: { id: c.client_id },
      });
      if (client !== undefined) {
        list.push(client);
      }
    }

    return list;
  }


}

export default GetContractsClientsService;
