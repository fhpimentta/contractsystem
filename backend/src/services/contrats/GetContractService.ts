import { getRepository } from 'typeorm';
import Contracts from '../../models/Contracts';

class GetContractsService {
  public async execute(_id: string): Promise<Contracts[]> {
    const contractRepository = getRepository(Contracts);

    const contracts =
      _id !== ''
        ?  [ await contractRepository.findOne({ where: { id: _id } })]
        : await contractRepository.find();

    return contracts;
  }
}

export default GetContractsService;
