import { Router } from 'express';
import CreateClientsService from '../services/clients/CreateClientService';
import UpdateClientsService from '../services/clients/UpdateClientService';
import DeleteClientsService from '../services/clients/DeleteClientService';
import GetClientsService from '../services/clients/GetClientsService';
import GetClientContractsService from '../services/clients/GetClientsContractsService';

const clientsRouter = Router();

clientsRouter.get('/', async (request, response) => {
  const getClient = new GetClientsService();
  const client = await getClient.execute('');

  return response.json(client);
});
clientsRouter.get('/:id', async (request, response) => {
  const getClient = new GetClientsService();
  const client = await getClient.execute(request.params.id);

  return response.json(client[0]);
});
clientsRouter.get('/contracts/:id', async (request, response) => {
  const getClientContracts = new GetClientContractsService();
  const contracts = await getClientContracts.execute(request.params.id);
  return response.json(contracts);
});
clientsRouter.post('/', async (request, response) => {
  const { name, surname, cpf, phone, email } = request.body;

  const createClient = new CreateClientsService();

  const client = await createClient.execute({
    name,
    surname,
    cpf,
    phone,
    email,
  });

  return response.json(client);
});

clientsRouter.put('/:id', async (request, response) => {
  const { name, surname, cpf, phone, email } = request.body;

  const updateClient = new UpdateClientsService();

  const client = await updateClient.execute({
    id: request.params.id,
    name,
    surname,
    cpf,
    phone,
    email,
  });

  return response.json(client);
});

clientsRouter.delete('/:id', async (request, response) => {
  const deleteClient = new DeleteClientsService();

  const client = await deleteClient.execute({
    id: request.params.id,
  });

  return response.json({ status: client });
});

export default clientsRouter;
