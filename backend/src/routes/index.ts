import { Router } from 'express';

import usersRouter from './users.routes';
import sessionsRouter from './sessions.routes';
import clientsRouter from './clients.routes';
import contractsRouter from './contracts.routes';

import ensureAuthenticated from '../middleware/ensureAuthenticated';

const routes = Router();

routes.use('/users', usersRouter);
routes.use('/sessions', sessionsRouter);

routes.use(ensureAuthenticated);
routes.use('/clients', clientsRouter);
routes.use('/contracts', contractsRouter);

export default routes;
