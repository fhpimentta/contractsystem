import { Router } from 'express';
import { parseISO } from 'date-fns';
import multer from 'multer';
import uploadConfig from '../config/upload';
import CreateContractsService from '../services/contrats/CreateContractService';
import ensureAuthenticated from '../middleware/ensureAuthenticated';
import UpdateContractFileService from '../services/contrats/UpdateContractFileService';
import UpdateContractService from '../services/contrats/UpdateContractService';
import DeleteContractService from '../services/contrats/DeleteContractService';
import GetContractsService from '../services/contrats/GetContractService';
import GetContractsClientsService from '../services/contrats/GetContractClientsService';

const contractsRouter = Router();
const upload = multer(uploadConfig);

contractsRouter.get('/', async (request, response) => {

  const getContract = new GetContractsService();

  const contract = await getContract.execute('');

  return response.json(contract);
});

contractsRouter.get('/:id', async (request, response) => {

  const getContract = new GetContractsService();

  const contract = await getContract.execute(request.params.id);

  return response.json(contract[0]);
});
contractsRouter.get('/clients/:id', async (request, response) => {

  const getContractClients = new GetContractsClientsService();

  const contract = await getContractClients.execute(request.params.id);

  return response.json(contract);
});
contractsRouter.post('/', async (request, response) => {
  const { title, clients, initial_date, final_date } = request.body;

  const createContract = new CreateContractsService();

  const i = parseISO(initial_date);
  const f = parseISO(final_date);

  const contract = await createContract.execute({
    title,
    initial_date: i,
    final_date: f,
    clients,
  });

  return response.json(contract);
});

contractsRouter.put('/:id', async (request, response) => {
  const { title, clients, initial_date, final_date } = request.body;

  const updateContract = new UpdateContractService();

  const i = parseISO(initial_date);
  const f = parseISO(final_date);

  const contract = await updateContract.execute({
    id: request.params.id,
    title,
    initial_date: i,
    final_date: f,
    clients,
  });

  return response.json(contract);
});



contractsRouter.delete('/:id', async (request, response) => {
  const deleteContract = new DeleteContractService();

  const contract = await deleteContract.execute({
    id: request.params.id,
  });

  return response.json({ status: contract });
});

contractsRouter.patch(
  '/file/:id',
  ensureAuthenticated,
  upload.single('file'),
  async (request, response) => {
    const updateContractFile = new UpdateContractFileService();

    const contract = await updateContractFile.execute({
      contract_id: request.params.id,
      filename: request.file.filename,
    });

    return response.json(contract);
  },
);

export default contractsRouter;
