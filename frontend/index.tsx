import React, { ChangeEvent, useCallback, useState, useRef } from 'react';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { FiUser } from 'react-icons/fi';
import { Container } from './styles';
import Input from '../../components/Input';
import Button from '../../components/Button';
import SelectContainer from '../../components/Select';
import DatePickerContainer from '../../components/InputDatePicker';

const Perfil: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const [startDate, setStartDate] = useState(new Date());



  const handlePreview = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    console.log(file);
    if (!file) {
     // setPreview(null);
    }
    const previewURL = URL.createObjectURL(file);
   // setPreview(previewURL);
  }, []);

  const handleSubimit = useCallback((data: object) => {
    console.log(data);

  }, []);
  return (
    <Container>
      <h1>Perfil</h1>

      <Form onSubmit={handleSubimit} ref={formRef} >
        <Input name="name" icon={FiUser} placeholder="Nome" />

        <Input type="file" onChange={handlePreview} name="file" icon={FiUser} placeholder="Nome" />
        <SelectContainer name="name" icon={FiUser} placeholder="Nome" />
        <DatePickerContainer
        
          setDataCallback={(date: Date) => setStartDate(date)}
        />
        <Button type="submit">Salvar</Button>
      </Form>
    </Container>
  );
};

export default Perfil;
