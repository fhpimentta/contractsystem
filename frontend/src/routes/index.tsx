import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Routes';
import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import Dashboard from '../pages/Dashboard';
import Clients from '../pages/Clients';
import ClientAdd from '../pages/Clients/add';
import ClientEdit from '../pages/Clients/edit';
import Contracts from '../pages/Contracts';
import ContractAdd from '../pages/Contracts/add';
import ContractEdit from '../pages/Contracts/edit';
import ClientsContract from '../pages/Contracts/clientsContract';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={SignIn} />
    <Route path="/signup" component={SignUp} />

    <Route path="/dashboard" isPrivate component={Dashboard} />
   
    <Route path="/clients/add" isPrivate component={ClientAdd} />
    <Route path="/clients/edit/:id" isPrivate component={ClientEdit} />
    <Route path="/clients/contracts/:id" isPrivate component={ClientsContract} />
    <Route path="/clients" isPrivate component={Clients} />

    <Route path="/contracts/add" isPrivate component={ContractAdd} />
    <Route path="/contracts/edit/:id" isPrivate component={ContractEdit} />
    <Route path="/contracts" isPrivate component={Contracts} />
    
  </Switch>
);

export default Routes;
