import React, {
  ChangeEvent,
  useCallback,
  useRef,
  useState,
  useEffect,
} from 'react';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { FiUser, FiArrowLeft, FiPenTool } from 'react-icons/fi';
import { Redirect, Link } from 'react-router-dom';
import Multiselect from 'react-widgets/lib/Multiselect';
import {
  Container,
  ContainerForm,
  ContainerSelect,
  ContainerLabel,
} from './styles';
import Input from '../../../components/Input';
import DatePickerContainer from '../../../components/InputDatePicker';
import Button from '../../../components/Button';
import api from '../../../services/api';
import 'react-widgets/dist/css/react-widgets.css';

interface ContractFormData {
  title: string;
}

interface ClientsForm {
  id: string;
  name: string;
  phone: string;
  email: string;
  surname: string;
}

const ContractAdd: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  
  const [redirect, setRedirect] = useState(false);
  const [clients, setClient] = useState(['']);
  const [startDate, setStartDate] = useState(new Date());
  const [finalDate, setFinalDate] = useState(new Date());
  const [clientsSelected, setClients] = useState(['']);
  const [clientsData, SetClientsData] = useState([] as ClientsForm[]);
  const [file, setFile] = useState();

  useEffect(() => {
    api.get('/clients').then((resp) => {
      const list: string[] = [];

      resp.data.forEach((client: any) => {
        list.push(client.name);
      });

      setClient(list);
      SetClientsData(resp.data);
    });
  }, []);

  const handleSubmit = useCallback(
    async (data: ContractFormData) => {
      const list: string[] = [];

      clientsData.forEach((client) => {
        if (clientsSelected.findIndex((c) => c === client.name)) {
          list.push(client.id);
        }
      });

      api
        .post('/contracts', {
          title: data.title,
          initial_date: startDate,
          final_date: finalDate,
          clients: list,
        })
        .then((resp) => {
          const formData = new FormData();
          formData.append('file', file);
          api
            .patch(`/contracts/file/${resp.data.id}`, formData, {
              headers: {
                'Content-Type': 'multipart/form-data',
              },
            })
            .then((resp) => {
              setRedirect(true);
            });
        });
    },
    [clientsSelected, file, clientsData, setRedirect, startDate, finalDate],
  );

  const handlePreview = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setFile(e.target.files?.[0]);
    },
    [setFile],
  );

  return (
    <Container>
      <Link to="/contracts">
        <FiArrowLeft /> Voltar para Contratos
      </Link>
      {redirect ? <Redirect to="/contracts" /> : null}
      <h1>Novo Contrato</h1>
      <ContainerForm>
        <Form onSubmit={handleSubmit} ref={formRef}>
          <ContainerLabel>Titulo</ContainerLabel>

          <Input name="title" icon={FiUser} placeholder="Titulo" />
          <ContainerLabel>Clientes</ContainerLabel>

          <ContainerSelect>
            <Multiselect
              data={clients}
              onChange={(value) => setClients(value)}
            />
          </ContainerSelect>
          <ContainerLabel>Data de início</ContainerLabel>

          <DatePickerContainer
            name="initial_date"
            value={startDate}
            setDataCallback={(date: Date) => setStartDate(date)}
          />
          <ContainerLabel>Data de final</ContainerLabel>

          <DatePickerContainer
            name="final_date"
            value={startDate}
            setDataCallback={(date: Date) => setFinalDate(date)}
          />
          <ContainerLabel>Contrato</ContainerLabel>

          <Input
            type="file"
            onChange={handlePreview}
            name="file"
            accept=".doc,.docx,application/pdf,application/msword"
            icon={FiPenTool}
            placeholder="Nome"
          />
          <Button type="submit" isError={false}>
            Salvar
          </Button>
        </Form>
      </ContainerForm>
    </Container>
  );
};

export default ContractAdd;
