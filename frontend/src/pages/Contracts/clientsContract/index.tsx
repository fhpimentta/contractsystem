import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { Link, useParams } from 'react-router-dom';
import { FiArrowLeft, FiPlus, FiEdit } from 'react-icons/fi';
import { Container, ContainerTable } from './styles';
import api from '../../../services/api';
import Button from '../../../components/Button';

interface ContractsType {
  id: string;
  title: string;
  initial_date: Date;
  final_date: Date;
}

const ClientsContract: React.FC = () => {
  const [data, setData] = useState();
  const { id } = useParams();
  useEffect(() => {
    api.get(`/clients/contracts/${id}`).then((resp) => {
      console.log(resp.data);
      setData(resp.data);
    });
  }, []);
  const columns = [
    {
      name: 'Titulo',
      selector: 'title',
      sortable: true,
    },
    {
      name: 'Início',
      selector: 'initial_date',
      sortable: true,
      cell: (row: ContractsType) => (
        <div>
          <div>
            {new Date(row.initial_date).getDate()}/
            {(new Date(row.initial_date).getMonth())+1}/
            {new Date(row.initial_date).getFullYear()}
          </div>
        </div>
      ),
    },
    {
      name: 'Final',
      selector: 'final_date',
      sortable: true,
      cell: (row: ContractsType) => (
        <div>
          <div>
            {new Date(row.final_date).getDate()}/
            {(new Date(row.final_date).getMonth())+1 }/
            {new Date(row.final_date).getFullYear()}
          </div>
        </div>
      ),
    },
    {
      name: 'Editar',
      cell: (row: ContractsType) => (
        <div>
          <div>
            
            <Link to={`/contracts/edit/${row.id}`}>
              <Button isError={false}><FiEdit /> </Button>
            </Link>
          </div>
        </div>
      ),
    },
    
  ];
  return (
    <Container>
      <Link to="/clients" >
          <FiArrowLeft /> Voltar para Clientes
        </Link>
      <ContainerTable>
        <Link to="/contracts/add">
          <Button  isError={false}>
            <FiPlus /> Adicionar Contrato
          </Button>
        </Link>
        <DataTable title="Contratos" columns={columns} data={data} />
      </ContainerTable>
    </Container>
  );
};

export default ClientsContract;
