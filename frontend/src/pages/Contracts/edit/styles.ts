import styled from 'styled-components';
import { shade } from 'polished';
import { primaryColor } from '../../../config/colors';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  margin-top: 16px;
  a {
    color: ${primaryColor};
    diplay: block;
    margin-top: 24px;
    text-decoration: none;
    transition: color 0.2s;

    &:hover {
      color: ${shade(0.2, primaryColor)};
    }
    svg {
      margin-right: 16px;
    }
`;
export const ContainerForm = styled.div`
  margin-top: 16px;
`;

export const ContainerSelect = styled.div`
  background: #232129;
  border-radius: 10px;
  border: 2px solid #232129;
  color: #666360;

  & + div {
    margin-top: 8px;
  }
`;

export const ContainerLink = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  a {
    color: #f4ede8;
    diplay: block;
    margin-top: 5px;
    margin-bottom: 15px;
    text-decoration: none;
    transition: color 0.2s;

    &:hover {
      color: ${shade(0.2, '#f4ede8')};
    }
  }
`;
export const ContainerLabel = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;

  color: #f4ede8;
  diplay: block;
  margin-top: 5px;
  margin-bottom: 5px;
  text-decoration: none;
  transition: color 0.2s;
`;
