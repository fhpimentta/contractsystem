import React, {
  ChangeEvent,
  useCallback,
  useRef,
  useState,
  useEffect,
} from 'react';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';

import { FiUser, FiArrowLeft } from 'react-icons/fi';
import { Redirect, useParams, Link } from 'react-router-dom';
import Multiselect from 'react-widgets/lib/Multiselect';
import {
  Container,
  ContainerForm,
  ContainerSelect,
  ContainerLink,
  ContainerLabel,
} from './styles';
import Input from '../../../components/Input';
import DatePickerContainer from '../../../components/InputDatePicker';
import Button from '../../../components/Button';
import api, { baseURL } from '../../../services/api';
import 'react-widgets/dist/css/react-widgets.css';

interface ContractFormData {
  title: string;
}

interface ClientsForm {
  id: string;
  name: string;
  phone: string;
  email: string;
  surname: string;
}

const ContractEdit: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const [redirect, setRedirect] = useState(false);
  const [clients, setClient] = useState(['']);
  const [startDate, setStartDate] = useState(new Date());
  const [finalDate, setFinalDate] = useState(new Date());
  const [clientsSelected, setClients] = useState(['']);
  const [clientsContractSelected, setContractClients] = useState(
    [] as string[],
  );
  const [clientsData, SetClientsData] = useState([] as ClientsForm[]);
  const [file, setFile] = useState();
  const [contractFile, setContractFile] = useState('');
  const [title, setTitle] = useState('');

  const { id } = useParams();

  useEffect(() => {
    api.get('/clients').then((resp) => {
      const list: string[] = [];
      resp.data.forEach((client: any) => {
        list.push(client.name);
      });
      setClient(list);
      SetClientsData(resp.data);
    });

    api.get(`/contracts/${id}`).then((respContract) => {
      setTitle(respContract.data.title);
      const i = new Date(Date.parse(respContract.data.initial_date));
      const f = new Date(Date.parse(respContract.data.final_date));
      setStartDate(i);
      setFinalDate(f);
      setContractFile(`${baseURL}/files/${respContract.data.file}`);

      api.get(`/contracts/clients/${id}`).then((clientsContracts) => {
        const listContract: string[] = [];
        clientsContracts.data.forEach((client: any) => {
          listContract.push(client.name);
        });

        setContractClients(listContract);
        setClients(listContract);
      });
    });
  }, []);

  const handleSubmit = useCallback(
    async (data: ContractFormData) => {
      const list: string[] = [];
    
      clientsData.forEach((client) => {
        const hasSelected = clientsSelected.find((c) => c === client.name);
        if (hasSelected !== undefined) {
          list.push(client.id);
        }
      });
     
      api
        .put(`/contracts/${id}`, {
          title: data.title,
          initial_date: startDate,
          final_date: finalDate,
          clients: list,
        })
        .then((resp) => {
          if (file !== undefined) {
            const formData = new FormData();
            formData.append('file', file);
            api
              .patch(`/contracts/file/${resp.data.id}`, formData, {
                headers: {
                  'Content-Type': 'multipart/form-data',
                },
              })
              .then((resp) => {
                setRedirect(true);
              });
          } else {
            setRedirect(true);
          }
        });
    },
    [clientsSelected, file, clientsData, setRedirect, startDate, finalDate],
  );

  const handlePreview = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setFile(e.target.files?.[0]);
    },
    [setFile],
  );
  const handleDelete = useCallback(() => {
    api.delete(`/contracts/${id}`).then((resp) => {
      setRedirect(true);
    });
  }, []);
  return (
    <Container>
         <Link to="/contracts">
        <FiArrowLeft /> Voltar para Contratos
      </Link>
      {redirect ? <Redirect to="/contracts" /> : null}
      <h1>Novo Contrato</h1>
      <ContainerForm>
        <Form
          onSubmit={handleSubmit}
          ref={formRef}
          initialData={{
            title,
            initial_date: startDate,
          }}
        >
          <ContainerLabel>Titulo</ContainerLabel>
          <Input name="title" icon={FiUser} placeholder="Titulo" />
          <ContainerLabel>Clientes</ContainerLabel>

          <ContainerSelect>
            {clientsContractSelected.length > 0 ? (
              <Multiselect
                defaultValue={clientsContractSelected}
                data={clients}
                onChange={(value) => setClients(value)
                
                }
              />
            ) : null}
          </ContainerSelect>
          <ContainerLabel>Data de início</ContainerLabel>
          <DatePickerContainer
            name="initial_date"
            value={startDate}
            setDataCallback={(date: Date) => {
              setStartDate(date);
            }}
          />
          <ContainerLabel>Data de final</ContainerLabel>
          <DatePickerContainer
            name="final_date"
            value={finalDate}
            setDataCallback={(date: Date) => setFinalDate(date)}
          />
          <ContainerLabel>Contrato</ContainerLabel>

          <Input
            type="file"
            accept=".doc,.docx,application/pdf,application/msword"
            onChange={handlePreview}
            name="file"
            icon={FiUser}
            placeholder="Nome"
          />
          {contractFile !== '' ? (
            <ContainerLink>
              <a href={contractFile} target="_blank">
                Visualizar Contrato
              </a>
            </ContainerLink>
          ) : null}
          <Button type="submit"  isError={false}>Atualizar</Button>
          <Button type="button"  isError={true} onClick={handleDelete}>
            Deletar
          </Button>
        </Form>
      </ContainerForm>
    </Container>
  );
};

export default ContractEdit;
