import React, { useCallback, useRef, useState, useEffect } from 'react';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import * as Yup from 'yup';
import {
  FiUser,
  FiMail,
  FiPhone,
  FiTerminal,
  FiUsers,
  FiArrowLeft,
} from 'react-icons/fi';
import { Redirect, useParams, Link } from 'react-router-dom';
import { Container, ContainerForm } from './styles';
import Input from '../../../components/Input';
import InputMask from '../../../components/InputMask';
import Button from '../../../components/Button';
import getValidationErrors from '../../../utils/getValidationErrors';
import { useToast } from '../../../hooks/toast';
import api from '../../../services/api';

interface ClientFormData {
  name: string;
  surname: string;
  cpf: string;
  email: string;
  phone: string;
}

const ClientEdit: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { addToast } = useToast();
  const [cpf, setCpf] = useState('');
  const [redirect, setRedirect] = useState(false);
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [surname, setSurname] = useState('');
  const [name, setName] = useState('');

  const { id } = useParams();

  useEffect(() => {
    api.get(`/clients/${id}`).then((resp) => {
      setCpf(resp.data.cpf);
      setPhone(resp.data.phone);
      setEmail(resp.data.email);
      setSurname(resp.data.surname);
      setName(resp.data.name);
    });
  }, []);

  const handleDelete = useCallback(() => {
    api.delete(`/clients/${id}`).then((resp) => {
      setRedirect(true);
    });
  }, []);

  const handleSubmit = useCallback(
    async (data: ClientFormData) => {
      try {
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          email: Yup.string()
            .required('Email obrigatório')
            .email('digite um email valido'),
          name: Yup.string().required('Nome obrigatório'),
          surname: Yup.string().required('Sobre Nome obrigatório'),
          cpf: Yup.string().min(14, 'Insira um cpf valido'),
          phone: Yup.string().min(18, 'Insira um telefone valido'),
        });

        const newData = {
          name: data.name,
          surname: data.surname,
          email: data.email,
          cpf: cpf.replace(' ', ''),
          phone: phone.replace(' ', ''),
        };

        await schema.validate(newData, { abortEarly: false });

        api.put(`/clients/${id}`, newData).then((resp) => {
          setRedirect(true);
        });

        addToast({
          type: 'success',
          title: 'Sucesso',
          description: 'Cadastro realizado com sucesso!',
        });
        // formRef.current?.reset();
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);
          formRef.current?.setErrors(errors);
        }
        addToast({
          type: 'error',
          title: 'Erro no cadastro',
          description:
            'Erro ao efetuar o cadastro, tente novamente mais tarde!',
        });
      }
    },
    [addToast, cpf, phone, setRedirect],
  );

  return (
    <Container>
      <Link to="/clients">
        <FiArrowLeft /> Voltar para Clientes
      </Link>
      {redirect ? <Redirect to="/clients" /> : null}
      <h1>Novo Cliente</h1>
      <ContainerForm>
        <Form
          onSubmit={handleSubmit}
          ref={formRef}
          initialData={{ name, cpf, surname, phone, email }}
        >
          <Input name="name" icon={FiUser} placeholder="Nome" />
          <Input name="surname" icon={FiUsers} placeholder="Sobre Nome" />
          <InputMask
            name="cpf"
            value={cpf}
            icon={FiTerminal}
            placeholder="CPF"
            mask="999.999.999-99"
            valueCall={(value) => {
              setCpf(value);
            }}
          />
          <InputMask
            name="phone"
            value={phone}
            icon={FiPhone}
            placeholder="Telefone"
            mask="+55 (99) 9 9999-9999"
            valueCall={(value) => {
              setPhone(value);
            }}
          />
          <Input name="email" icon={FiMail} placeholder="Email" />

          <Button type="submit" isError={false}>
            Salvar
          </Button>
          <Button type="button" isError onClick={handleDelete}>
            Deletar
          </Button>
        </Form>
      </ContainerForm>
    </Container>
  );
};

export default ClientEdit;
