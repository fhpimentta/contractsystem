import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { Link } from 'react-router-dom';
import { FiArrowLeft, FiPlus, FiEdit, FiPenTool } from 'react-icons/fi';
import { Container, ContainerTable } from './styles';
import api from '../../services/api';
import Button from '../../components/Button';

interface ClientsType {
  id: string;
  name: string;
  email: string;
  cpf: string;
}

const Clients: React.FC = () => {
  const [data, setData] = useState();
  
  useEffect(() => {
    api.get('/clients').then((resp) => {
      console.log(resp);
      setData(resp.data);
    });
  }, []);
  const columns = [
    {
      name: 'Nome',
      selector: 'name',
      sortable: true,
    },
    {
      name: 'Email',
      selector: 'email',
      sortable: true,
      right: true,
    },
    {
      name: 'CPF',
      selector: 'cpf',
      sortable: true,
      right: true,
    },
    {
      name: 'Editar',
      cell: (row: ClientsType) => (
        <div>
          <div>
            <Link to={`/clients/edit/${row.id}`}>
              <Button isError={false}><FiEdit /> </Button>
            </Link>
          </div>
        </div>
      ),
    },
    {
      name: 'Contratos',
      cell: (row: ClientsType) => (
        <div>
          <div>
            <Link to={`/clients/contracts/${row.id}`}>
              <Button isError={false}><FiPenTool /> </Button>
            </Link>
          </div>
        </div>
      ),
    },
  ];
  return (
    <Container>
       <Link to="/dashboard" >
          <FiArrowLeft /> Voltar para Dashboard
        </Link>
      <ContainerTable>
        <Link to="/clients/add">
          <Button isError={false}>
            <FiPlus /> Adicionar Clientes
          </Button>
        </Link>
        <DataTable title="Clientes" columns={columns} data={data} />
      </ContainerTable>
    </Container>
  );
};

export default Clients;
