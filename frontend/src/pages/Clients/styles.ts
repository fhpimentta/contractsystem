import styled from 'styled-components';
import { primaryColor } from '../../config/colors';
import { shade } from 'polished';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  margin-top: 16px;
  a {
    color: ${primaryColor};
    diplay: block;
    margin-top: 24px;
    text-decoration: none;
    transition: color 0.2s;
    
    &:hover {
      color: ${shade(0.2, primaryColor)};
    }
    
  }
`;


export const ContainerTable = styled.div`
  margin-left: 100px;
  margin-right: 100px;
  width: 1000px;
`;

