import React, { useCallback, useRef } from 'react';
import { Link } from 'react-router-dom';
import { FiArrowLeft, FiMail, FiLock } from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import * as Yup from 'yup';
import { Container, Content, Background, AnimationContainer, Logo } from './styles';
import logoImg from '../../assets/logo.png';
import Input from '../../components/Input';
import Button from '../../components/Button';
import { useAuth } from '../../hooks/auth';
import getValidationErrors from '../../utils/getValidationErrors';
import { useToast } from '../../hooks/toast';

interface SignFormData {
  email: string;
  password: string;
}

const SignUp: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  
  const { signUp } = useAuth();
  const { addToast } = useToast();

  const handleSubmit = useCallback(
    async (data: SignFormData) => {
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          email: Yup.string()
            .required('Email obrigatório')
            .email('digite um email valido'),
          password: Yup.string().min(6, 'senha deve conter 6 digitos'),
        });
        await schema.validate(data, { abortEarly: false });
        await signUp({
          email: data.email,
          password: data.password,
        });
        addToast({
          type: 'success',
          title: 'Sucesso',
          description: 'Cadastro realizado com sucesso!',
        });
        formRef.current?.reset();
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);
          formRef.current?.setErrors(errors);
        }
        addToast({
          type: 'error',
          title: 'Erro no cadastro',
          description:'Erro ao efetuar o cadastro, tente novamente mais tarde!',
        });
      }
    },
    [SignUp,addToast],
  );

  return (
    <Container>
      <Background />

      <Content>
        <AnimationContainer>
          <Logo>
          <img src={logoImg} alt="logo"  />
          </Logo>
          <Form initialData={{}} onSubmit={handleSubmit} ref={formRef}>
            <h1>Cadastre-se</h1>
            <Input name="email" icon={FiMail} placeholder="Email" />
            <Input
              name="password"
              type="password"
              icon={FiLock}
              placeholder="Senha"
            />
            <Button type="submit"  isError={false} > Cadastrar</Button>
          </Form>
          <Link to="/">
            <FiArrowLeft /> Voltar para Logon
          </Link>
        </AnimationContainer>
      </Content>
    </Container>
  );
};
export default SignUp;
