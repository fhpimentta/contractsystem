import React, { useCallback } from 'react';
import { FiArrowLeft, FiUser, FiPaperclip } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import { Container, ButtonContainer } from './styles';
import Button from '../../components/Button';
import { useAuth } from '../../hooks/auth';

const Dashboard: React.FC = () => {
  const { signOut } = useAuth();
  const handleLogout = useCallback(() => {
    signOut();
  }, [signOut]);

  return (
    <Container>
      <h1>Bem vindo ao Dashboard</h1>
      <ButtonContainer>
        <Link to="/clients">
          <Button isError={false}>
            <FiUser /> Clientes
          </Button>
        </Link>
        <Link to="/contracts">
          <Button isError={false}>
            <FiPaperclip /> Contratos
          </Button>
        </Link>
        <Link to="/" onClick={handleLogout}>
          <FiArrowLeft /> Voltar para Logon
        </Link>
      </ButtonContainer>
    </Container>
  );
};

export default Dashboard;
