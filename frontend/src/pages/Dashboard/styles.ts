import styled from 'styled-components';
import { shade } from 'polished';
import { primaryColor } from '../../config/colors';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  margin-top: 16px;

  a {
    color: ${primaryColor};
    diplay: block;
    margin-top: 24px;
    text-decoration: none;
    transition: color 0.2s;
    
    &:hover {
      color: ${shade(0.2, primaryColor)};
    }
    svg {
      margin-right: 16px;
    }
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
`;
