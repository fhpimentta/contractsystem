import axios from 'axios';

export const baseURL = 'http://localhost:3333';
const api = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `Bearer ${localStorage.getItem('@Contracktor:token')}`,
  },
});

export default api;
