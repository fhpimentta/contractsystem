import React from 'react';
import DatePicker from 'react-datepicker';
import { Container } from './styles';
import 'react-datepicker/dist/react-datepicker.css';

interface DatePickerData {
  setDataCallback(date: Date): void;
  name: string;
  value: Date;
}

const DatePickerContainer: React.FC<DatePickerData> = ({
  setDataCallback,
  name,
  value,
  ...rest
}) => {
  return (
    <Container>
      <DatePicker
        name={name}
        selected={value}
        onChange={(date: Date) => {
          setDataCallback(date);
        }}
      />
    </Container>
  );
};

export default DatePickerContainer;
