import React, {  useEffect } from 'react';
import {
  FiAlertCircle,
  FiXCircle,
  FiCheckCircle,
  FiInfo,
} from 'react-icons/fi';
import { Container } from './styles';
import { ToastMessage, useToast } from '../../../hooks/toast';

interface ToastProps {
  msg: ToastMessage;
  style:object;
}

const icons = {
  info: <FiInfo size={24} />,
  success : <FiAlertCircle size={24} />,
  error : <FiCheckCircle size={24} />,
};

const Toast: React.FC<ToastProps> = ({ msg, style }) => {
  const { removeToast } = useToast();

  useEffect(() => {
    const timer = setTimeout(() => {
      removeToast(msg.id);
    }, 3000);
    return () => {
      clearTimeout(timer);
    };
  }, [msg.id, removeToast]);

  return (
    <Container
      style={style}
      type={msg.type}
      hasDescriptions={!!msg.description}
    >
      {icons[msg.type || 'info']}
      <div>
        <strong>{msg.title}</strong>
        <p>{msg.description}</p>
      </div>
      <button type="button" onClick={() => removeToast(msg.id)}>
        <FiXCircle size={18} />
      </button>
    </Container>
  );
};

export default Toast;
