import styled from 'styled-components';
import { shade } from 'polished';
import { primaryColor, dangerColor } from '../../config/colors';

interface ContainerProps {
  isErrored: boolean;
}

export const Container = styled.button<ContainerProps>`
  background: ${(props) => (props.isErrored ? dangerColor : primaryColor)};
  height: 56px;
  border-radius: 10px;
  border: 0px solid #232129;
  padding: 0 16px;
  color: #fff;
  width: 100%;
  font-weight: 500;
  margin-top: 16px;
  margin-bottom: 16px;

  transition: background-color 0.2s;
  &:hover {
    background: ${shade(0.2, primaryColor)};
  }
`;
