import React, { ButtonHTMLAttributes } from 'react';
import { Container } from './styles';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  isError: boolean;
}

const Button: React.FC<ButtonProps> = ({ children, isError, ...rest }) => (
  <Container type="button" isErrored={isError} {...rest}>
    {children}
  </Container>
);

export default Button;
